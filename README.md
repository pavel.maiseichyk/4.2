# Hello there!

_Paragraf 1_

__Paragraf 2__

~~Paragraf 3~~


> jest to cytat

- Zagnieżdżony element nienumerycny
- Zagnieżdżony element nienumerycny

1. Zagnieżdżony element numerycny
2. Zagnieżdżony element numerycny

```py
print("Hello There!")
print("General Kenobi")
print("You're a bold one.")
```

Możemy wpisać tu `print("Viva la diva")` i to będzie pięknie wyglądać!

![picture/pic.jpeg](pic.jpeg)
